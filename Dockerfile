FROM gitlab-registry.cern.ch/sft/docker:lcg-ubuntu18
LABEL maintainer="johannes.martin.heinz@cern.ch"

USER root

RUN apt-get update \
    && apt-get install -y curl s3cmd jq \
    && rm -rf /var/lib/apt/lists/*

RUN cd /tmp \
    && curl -o golang.tar.gz https://dl.google.com/go/go1.12.1.linux-amd64.tar.gz \
    && apt-get purge -y curl \
    && tar -xzf golang.tar.gz \
    && rm -rfv golang.tar.gz

RUN mkdir -p /tmp/gopath/src/github.com/cvmfs \
    && cd /tmp/gopath/src/github.com/cvmfs \
    && git clone https://github.com/cvmfs/conveyor.git \
    && cd conveyor \
    && /tmp/go/bin/go build \
    && mv /tmp/gopath/src/github.com/cvmfs/conveyor/conveyor /usr/local/bin/ \
    && rm -rf /tmp/go \
    && rm -rf /tmp/gopath \
    && mkdir -p /etc/cvmfs/conveyor
    
COPY config.toml /etc/cvmfs/conveyor/config.toml

USER sftnight
WORKDIR /build/jenkins/workspace
CMD ["/bin/bash"]
